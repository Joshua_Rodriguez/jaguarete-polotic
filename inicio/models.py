from django.contrib.auth.models import User
from django.db import models

class Categoria(models.Model):
    nombre= models.CharField(max_length=50)
    
    
    def __str__(self):
        return self.nombre

class Producto(models.Model):
    titulo=models.CharField(max_length=50)
    descripcion=models.TextField(null=True, blank=True, )
    precio= models.IntegerField()
    categoria=models.ForeignKey(Categoria, on_delete=models.PROTECT)
    fecha= models.DateField()
    imagen= models.ImageField("productos/")


    
    def __str__(self):
        return self.titulo


