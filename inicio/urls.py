from django.urls import path
from . import views




urlpatterns = [
    path('', views.render_index, name="index"),
    path('inicio/', views.render_index, name="inicio"),
    path('sobre_nosotros/', views.about, name="about"),
    path('registro/', views.registro , name='registro' ),
    path('rendernew/', views.rendernew, name='rendernew'),
    path('listar/', views.listar_producto, name="listar" ),
    path('modificar-producto/<id>/', views.modificar_producto, name="modificar-producto"),
    path('eliminar/<id>/', views.eliminar_obj, name="eliminar"),
    path('busqueda/', views.busqueda, name="busqueda"),
    path('categoria/<int:categoria_id>', views.categoria, name="categoria"),
    
    

]
