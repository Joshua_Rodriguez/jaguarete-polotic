from django import http
from django.core import paginator
from django.http import request
from django.http.response import Http404
import sweetify
from django.db.models import Q
from .models import Categoria, Producto
from django.shortcuts import redirect, render, get_object_or_404
from .forms import CustomUserCreationForm, Productoforms
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.core.paginator import Paginator
from django.contrib.auth.decorators import permission_required
# Create your views here.

def render_index (request):
    productos= Producto.objects.all()[0:3]
    data={
        'productos': productos
    }

    # messages.add_message(request, messages.INFO, 'Hello world')
    return render(request, 'inicio/index.html', data )
        
    

def about(request):
    return render(request, 'inicio/about.html', {
        'title':'Sobre nosotros'
    })

def render_login(request):
    return render(request, 'registration/login.html')


def registro(request):

    data={
        'form': CustomUserCreationForm()
    }

    if request.method== 'POST':
        formulario= CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user= authenticate(request, username= formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"])
            login(request, user)
            sweetify.success(request, 'Felicidades', text = 'Se ha registrado con exitos', icon='success' )
            return redirect(to= 'inicio')
        data["form"] = formulario

    return render(request, 'registration/registro.html', data)



@permission_required('inicio.add_producto')
def rendernew(request):

    data={
        'form': Productoforms()
    }

    if request.method== 'POST':
        formulario=Productoforms(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            sweetify.success(request, 'Felicidades', text = 'Su producto fue agregado', icon='success' )
        

        else:
            data["form"]=formulario

    return render(request, 'producto/agregar.html', data)

@permission_required('inicio.view_producto')
def listar_producto(request):
    productos= Producto.objects.all()
    page= request.GET.get('page',1)

    try:
        paginator=Paginator(productos, 5)
        productos=paginator.page(page)

    except:
        raise Http404


    data={
        'entity': productos,
        'paginator':paginator
    
    }


    return render(request, 'producto/lista.html', data)


@permission_required('inicio.change_producto')
def modificar_producto(request, id):


    producto = get_object_or_404(Producto, id=id)

    data={
        'form': Productoforms(instance=producto)
    }

    if request.method== 'POST':
        formulario = Productoforms(data=request.POST, instance=producto, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            sweetify.success(request, 'Felicidades', text = 'Se ha modificado', icon='success' )
            return redirect(to="listar")

        data["form"] = formulario

    return render(request, 'producto/modificar.html', data)

@permission_required('inicio.delete_producto')
def eliminar_obj(request, id):
    producto= get_object_or_404(Producto, id=id)
    producto.delete()
    sweetify.success(request, 'Elimino', text = 'Su producto ', icon='success' )
    return redirect(to="listar")



def busqueda(request):
    q = request.GET.get('q')

    querys = (Q(titulo__contains=q)| Q(categoria__nombre__contains=q))
    
    
    
    productos = Producto.objects.all().filter(querys)
    
    return render(request, 'inicio/busqueda.html', {'productos': productos})
    
    


def categoria(request, categoria_id):
    
    categoria= Categoria.objects.get(id=categoria_id)
    
    productos= Producto.objects.filter(categoria=categoria)
    data={
        'productos': productos,
        'categoria': categoria
    
    }

    return render(request,"inicio/categorias.html", data)



    
    